{
  lib,
  fetchFromGitHub,
  bamf,
  appmenu-gtk-module,
  keybinder3,
  libdbusmenu-gtk2,
  libdbusmenu-gtk3,
  libsForQt5,
  gtk3,
  python3Packages,
  gobject-introspection,
  wrapGAppsHook,
}:
python3Packages.buildPythonPackage {
  pname = "Fildem";
  version = "master";
  src = fetchFromGitHub {
    owner = "gonzaarcr";
    repo = "Fildem";
    rev = "dcd77d170822024ed7fc3c3781b3b77d1f076183";
    sha256 = "VSsVy6Q77gqBUxJjStaetbjGexUQdvGGUnNzRFCyoB4=";
  };
  nativeBuildInputs = [
    wrapGAppsHook
    gobject-introspection
  ];
  buildInputs = [
    appmenu-gtk-module
    gtk3
    bamf
    libdbusmenu-gtk2
    libdbusmenu-gtk3
    libsForQt5.libdbusmenu
  ];
  propagatedBuildInputs = with python3Packages;
    [
      setuptools
      pygobject3
    ]
    ++ [keybinder3];

  meta = with lib; {
    description = "MacOS style menu for gnome";
    license = licenses.gpl3;
  };
}
