final: prev: {
  appmenu-gtk-module = prev.callPackage ./libraries/appmenu-gtk-module {};
  colloid-gtk-theme-local = prev.callPackage ./theming/colloid-gtk-theme {};
  fildem =
    prev.callPackage ./gnome/fildem {
    };
  ### EXTREMELY EVIL, REMOVE IF NOT CERTAIN ###
  #optimizeWithFlag = pkg: flag:
  #pkg.overrideAttrs (attrs: {
  #NIX_CFLAGS_COMPILE = (attrs.NIX_CFLAGS_COMPILE or "") + " ${flag}";
  #});
  #stdenv = prev.impureUseNativeOptimizations prev.stdenv;
}
