{
  pkgs,
  config,
  lib,
  ...
}: {
  i18n = {
    defaultLocale = "en_GB.UTF-8";
  };
  time.timeZone = "Europe/Helsinki";

  users.users.${config.system.username} = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = ["adbusers" "input" "networkmanager" "plugdev" "wheel"];
  };
  environment.shells = [
    pkgs.zsh
  ];
  security.sudo.wheelNeedsPassword = false;
  # enable zsh autocompletion for system packages (systemd, etc)
  environment.pathsToLink = ["/share/zsh"];

  hardware = {
    video.hidpi.enable = lib.mkDefault true;
    enableRedistributableFirmware = true;
    opengl = {
      driSupport = true;
      driSupport32Bit = true;
      enable = true;
    };
    cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
  };
  boot = {
    tmpOnTmpfs = true;
    initrd.systemd.enable = true;
    cleanTmpDir = true;
    loader = {
      systemd-boot.enable = true;
      systemd-boot.editor = true;
      efi.canTouchEfiVariables = true;
    };
  };

  # DO NOT TOUCH
  system.stateVersion = "23.05";
  home-manager.users.${config.system.username}.home.stateVersion = config.system.stateVersion;
}
