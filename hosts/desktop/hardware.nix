{
  config,
  lib,
  pkgs,
  ...
}: {
  hardware = {
    opengl = {
      extraPackages = with pkgs; [
        rocm-opencl-icd
        rocm-opencl-runtime
        amdvlk
      ];
      extraPackages32 = with pkgs; [
        driversi686Linux.amdvlk
      ];
    };
  };
  environment.variables.AMD_VULKAN_ICD = lib.mkDefault "RADV";
  boot = {
    initrd = {
      kernelModules = ["amdgpu" "cpuid" "dm-snapshot"];
      availableKernelModules = ["vmd" "xhci_pci" "ahci" "nvme" "usbhid" "usb_storage" "sd_mod"];
    };
    kernelModules = ["amdgpu" "kvm-intel"];
    kernelPackages = pkgs.linuxPackages_latest;
    kernelParams = [
      "quiet"
      "splash"
      "amdgpu.ppfeaturemask=0xffffffff"
      "rd.systemd.show_status=false"
      "rd.udev.log_level=3"
      "udev.log_priority=3"
      "usbcore.autosuspend=1800"
    ];
  };
}
