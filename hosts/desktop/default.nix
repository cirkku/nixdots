{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: {
  config = {
    console.keyMap = "us";
    system = {
      username = "tuukka";
      programs = {
        firefox.enable = true;
        helix.enable = true;
        zsh.enable = true;
        git.enable = true;
        direnv.enable = true;
      };
      desktop = {
        gnome.enable = true;
        gtk.enable = true;
      };
      services = {
        ssh = {
          enable = true;
          extraSecurity = true;
          port = 1337;
        };
        mpd.enable = true;
        pipewire.enable = true;
      };
    };
    services.power-profiles-daemon.enable = lib.mkForce false;
    security.rtkit.enable = true;
    services.thermald.enable = true;
    hardware.pulseaudio.enable = false;
    programs.steam.enable = true;
    programs.gamemode.enable = true;
  };
}
