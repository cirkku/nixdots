{
  config,
  pkgs,
  ...
}: {
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/f57ea954-bc97-40a4-9ac2-bfb61b9ceff9";
    fsType = "ext4";
  };
  fileSystems."/home/${config.system.username}" = {
    device = "/dev/disk/by-uuid/1a6a114f-bf73-4da2-a936-0b4e1a9ccbe5";
    fsType = "ext4";
  };
  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/B43D-2CC9";
    fsType = "vfat";
  };
  swapDevices = [
    {device = "/dev/disk/by-uuid/371d8ce6-648d-4002-8226-9d9863d4576c";}
  ];
}
