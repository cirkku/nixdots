# Hosts
Configures all options set in `modules/default.nix` as well as other machine specific settings.

Name         | Description
------------ | -----------
`desktop`    | main machine. basic settings.
`laptop`     | enables powersaving settings and LUKS.

All the hosts have a shared config in `modules/minimal.nix`.

