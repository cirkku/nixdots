{
  config,
  pkgs,
  ...
}: {
  boot.initrd.luks.devices.crypt = {
    device = "/dev/disk/by-id/nvme-KINGSTON_SA2000M8250G_50026B72823F1BB4-part2";
  };
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/4d867f55-2144-49ae-bf91-23936ea5f875";
    fsType = "xfs";
  };

  fileSystems."/home" = {
    device = "/dev/disk/by-uuid/fe977f90-fe61-40a8-9f23-2e824dc1031c";
    fsType = "xfs";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/C313-B195";
    fsType = "vfat";
  };
  swapDevices = [
    {device = "/dev/disk/by-uuid/088827c0-d82d-4312-bf7a-9142f86cf49d";}
  ];
}
