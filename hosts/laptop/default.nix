{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: {
  config = {
    console.keyMap = "fi";
    system = {
      gnomede = {
        enable = true;
        office = true;
        minimal = true;
        fonts = true;
      };
      pipewire = {
        enable = true;
        rt = false;
      };
      ssh = {
        enable = false;
        port = 1337;
      };
      firefox.enable = true;
      helix.enable = true;
      zsh.enable = true;
      git.enable = true;
      direnv.enable = true;
      xdg.enable = true;
      packages = {
        essential = true;
        gaming = false;
      };
    };
    networking = {
      networkmanager = {
        enable = true;
        dns = "systemd-resolved";
        wifi.powersave = true;
      };
    };
    services.power-profiles-daemon.enable = true;
  };
}
