<h1 align="center">dotfiles</h1>

# About

Configs for NixOS and home manager as a module.
Using:
- [flakes](https://nixos.wiki/wiki/Flakes)
- [flake-parts](https://github.com/gytis-ivaskevicius/flake-utils-plus).

List all flake outputs by running:
`nix flake show .#` on the cloned archive.

## Contents

- [modules](modules): Modules for configuring various services via NixOS modules and the home-manager module.
- [hosts](hosts): host-specific configuration such as filesystems and default user groups.
- [pkgs](pkgs): overlay packages. currently only an up to date version of my preferred GTK theme.
# Packages

Run packages directly with:

```console
nix run .#system.packageName
```

Or install from the `packages` output:

```nix
# flake.nix
{
  inputs.nixdots.url = "git+https://gitlab.com/cirkku/nixdots";
}

# configuration.nix
{pkgs, inputs, ...}: {
  environment.systemPackages = [
    inputs.nixdots.packages."x86_64-linux".packageName
  ];
}
```

## Desktop preview
I'm using just regular old GNOME so there is not much to see.
<p align="center">
  <img src="preview.png">
</p>

# Resources

Configurations from where I learned and copied:
- [fufexan/dotfiles](https://github.com/fufexan/dotfiles)
- [notusknot/dotfiles-nix](https://github.com/notusknot/dotfiles-nix)
- [gytis-ivaskevicius/nixfiles](https://github.com/gytis-ivaskevicius/nixfiles)
- [NobbZ/nixos-config](https://github.com/NobbZ/nixos-config)
