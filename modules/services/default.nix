{
  pkgs,
  lib,
  config,
  inputs,
  ...
}:
with lib; let
  cfg = config.system.services;
  username = config.system.username;
in {
  config = {
    ### config.system.services.pipewire
    services.pipewire = mkIf cfg.pipewire.enable {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };
    ### config.system.services.ssh
    services.sshd.enable = cfg.ssh.enable;
    services.openssh = mkIf cfg.ssh.enable {
      settings.passwordAuthentication = false;
      startWhenNeeded = true;
      listenAddresses = [
        {
          addr = "0.0.0.0";
          port = cfg.ssh.port;
        }
      ];
    };
    networking.firewall.allowedTCPPorts = mkIf cfg.ssh.extraSecurity [cfg.ssh.port];
    services.fail2ban.enable = cfg.ssh.extraSecurity;
    ### config.system.services.mpd
    services.mpd = {
      enable = cfg.mpd.enable;
      user = "${username}";
      musicDirectory = "/home/${username}/Music/";
      dataDir = "/home/${username}/.mpd/DataDir";
      extraConfig = ''
        audio_output {
          type "pipewire"
          name "piippujohto"
        }
        audio_output {
          type "fifo"
          name "my_fifo"
          path "/tmp/mpd.fifo"
          format "44100:16:2"
        }
      '';
    };
    systemd.services.mpd.environment = mkIf cfg.mpd.enable {
      XDG_RUNTIME_DIR = "/run/user/1000";
    };
  };
}
