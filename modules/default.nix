{
  config,
  lib,
  ...
}:
with lib; {
  imports = [
    #services
    ./services
    #home
    ./home
    #desktop environments
    ./desktop
    ./gtk
    #misc
    ./packages
  ];
  options.system = {
    username = mkOption {
      type = types.str;
      description = "default username";
      default = "tuukka";
    };
    desktop = {
      gnome.enable = mkEnableOption "enable the gnome desktop environment";
      gtk.enable = mkEnableOption "set a custom gtk theme";
    };
    programs = {
      firefox.enable = mkEnableOption "enable firefox";
      helix.enable = mkEnableOption "enable the helix text editor";
      zsh.enable = mkEnableOption "enable z shell";
      git.enable = mkEnableOption "enable git with settings";
      direnv.enable = mkEnableOption "enable direnv environments";
    };
    services = {
      mpd.enable = mkEnableOption "enable music player daemon";
      pipewire.enable = mkEnableOption "enable pipewire";
      ssh = {
        enable = mkEnableOption "enable openssh and sshd";
        extraSecurity = mkEnableOption "enable fail2ban and firewall ports";
        port = mkOption {
          type = types.int;
          description = "port that the ssh daemon will open in";
        };
      };
    };
  };
}
