{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:
with lib; let
  cfg = config.system.desktop;
in {
  config = {
    services.xserver = mkIf cfg.gnome.enable {
      enable = true;
      layout = "us,fi,rs,ru";
      xkbOptions = "eurosign:e,grp:alt_space_toggle,caps:ctrl_modifier";
      desktopManager.gnome.enable = true;
      displayManager.gdm = {
        enable = true;
        wayland = true;
      };
      #cool kids don't need mouse acceleration
      libinput = {
        enable = true;
        touchpad = {
          accelProfile = "flat";
          accelSpeed = "0";
        };
      };
    };
    fonts.fonts = with pkgs; [
      noto-fonts-cjk
      noto-fonts
      noto-fonts-extra
      noto-fonts-emoji
      jetbrains-mono
      roboto
      (nerdfonts.override {fonts = ["JetBrainsMono"];})
    ];
  };
  imports = [
    ./gnome
  ];
}
