{
  config,
  pkgs,
  inputs,
  lib,
  ...
}:
with lib; {
  config = mkIf config.system.desktop.gnome.enable {
    programs.gnome-terminal.enable = true;
    environment.gnome.excludePackages = [
      pkgs.gnome.geary
      pkgs.gnome.cheese
      pkgs.gnome.gedit
      pkgs.gnome.gnome-music
      pkgs.gnome-console
      pkgs.epiphany
      pkgs.gnome.gnome-characters
      pkgs.gnome.tali
      pkgs.gnome.iagno
      pkgs.gnome.hitori
      pkgs.gnome.atomix
      pkgs.gnome-tour
      pkgs.evince
    ];
    home-manager.users.${config.system.username} = {
      home.sessionVariables = {
        GTK_THEME = "Colloid-Teal-Dark-Nord";
        NIXOS_OZONE_WL = "1";
        QT_QPA_PLATFORM = "wayland";
        EDITOR = "hx";
        TERMINAL = "gnome-terminal";
        BROWSER = "firefox";
        NIX_CONFIG_DIR = "$HOME/.config/nixfiles/";
        MOZ_ENABLE_WAYLAND = "1";
      };
      home.packages = with pkgs; [
        #Gnome
        gnome.gnome-tweaks
        gnomeExtensions.blur-my-shell
        gnomeExtensions.noannoyance-2
        gnomeExtensions.hide-activities-button
        gnomeExtensions.vitals
        gnomeExtensions.dash-to-dock
        gnomeExtensions.no-overview
        gnomeExtensions.gsconnect
        #Media
        celluloid
        mpdevil
        foliate
        mousai
        obs-studio
        gimp
        #Piracy
        transmission-gtk
        nicotine-plus
      ];
      dconf.settings = (import ./dconf.nix) inputs;
    };
  };
}
