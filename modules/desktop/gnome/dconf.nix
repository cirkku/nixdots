inputs: let
  gvariant = inputs.home-manager.lib.hm.gvariant;
in {
  "io/github/celluloid-player/celluloid" = {
    mpv-config-enable = true;
    mpv-config-file = "file://${./mpv.conf}";
    mpv-input-config-enable = true;
    mpv-input-config-file = "file://${./input.conf}";
    settings-migrated = true;
  };
  "org/gnome/desktop/background" = {
    picture-uri = "file://${./wallpaper.jpg}";
  };
  "org/gnome/desktop/screensaver" = {
    picture-uri = "file://${./wallpaper.jpg}";
  };
  "org/gnome/desktop/input-sources" = {
    sources = [
      (gvariant.mkTuple ["xkb" "us"])
      (gvariant.mkTuple ["xkb" "fi"])
      (gvariant.mkTuple ["xkb" "ru"])
      (gvariant.mkTuple ["xkb" "rs"])
    ];
    xkb-options = ["eurosign:e" "grp:alt_space_toggle" "caps:ctrl_modifier"];
  };
  "org/gnome/desktop/peripherals/mouse" = {
    speed = -0.7424892703862661;
  };
  "org/gnome/desktop/peripherals/touchpad" = {
    two-finger-scrolling-enabled = true;
    tap-to-click = true;
  };
  "org/gnome/desktop/wm/keybindings" = {
    switch-applications = ["<Alt>Tab"];
    switch-applications-backward = ["<Shift><Alt>Tab"];
    switch-windows = ["<Super>Tab"];
    switch-windows-backward = ["<Shift><Super>Tab"];
  };
  "org/gnome/desktop/wm/preferences" = {
    button-layout = "appmenu:minimize,maximize,close";
  };
  "org/gnome/shell" = {
    disable-user-extensions = false;
    disabled-extensions = [];
    enabled-extensions = [
      "dash-to-dock@micxgx.gmail.com"
      "Hide_Activities@shay.shayel.org"
      "no-overview@fthx"
      "native-window-placement@gnome-shell-extensions.gcampax.github.com"
      "noannoyance@daase.net"
      "user-theme@gnome-shell-extensions.gcampax.github.com"
      "Vitals@CoreCoding.com"
      "drive-menu@gnome-shell-extensions.gcampax.github.com"
      "gsconnect@andyholmes.github.io"
      "blur-my-shell@aunetx"
    ];
    favorite-apps = [
      "firefox.desktop"
      "org.gnome.Terminal.desktop"
      "org.gnome.Nautilus.desktop"
      "org.gnome.Calendar.desktop"
      "org.gnome.Photos.desktop"
      "webcord.desktop"
    ];
  };
  "org/gnome/shell/extensions/blur-my-shell/overview" = {
    blur = true;
  };
  "org/gnome/shell/extensions/blur-my-shell/panel" = {
    blur = false;
  };
  "org/gnome/shell/extensions/dash-to-dock" = {
    apply-custom-theme = false;
    background-color = "rgb(42,45,52)";
    background-opacity = 0.75;
    custom-background-color = true;
    custom-theme-shrink = true;
    dash-max-icon-size = 32;
    dock-position = "BOTTOM";
    height-fraction = 1.0;
    running-indicator-style = "DOTS";
    transparency-mode = "FIXED";
  };
  "org/gnome/shell/extensions/user-theme" = {
    name = "Colloid-Teal-Dark-Nord";
  };
  "org/gnome/shell/extensions/vitals" = {
    hot-sensors = ["_memory_usage_" "_system_load_1m_" "__network-rx_max__" "__temperature_avg__"];
  };
}
