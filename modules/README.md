# Modules

Name           | Description
-------------- | -----------
`default.nix`  | option definitions and sources the other modules.
Gnome          | GNOME config
Packages       | Packages for basic system usage as well as services for gaming.
Nix            | Nix-related options
Services       | System and user services. PipeWire, MPD, SSH
