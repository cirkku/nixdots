{
  config,
  lib,
  inputs,
  pkgs,
  ...
}: {
  home-manager.users.${config.system.username} = {
    programs = {
      firefox = import ./firefox {inherit pkgs lib config;};
      direnv = import ./direnv {inherit pkgs lib config;};
      git = import ./git {inherit pkgs lib config;};
      zsh = import ./zsh {inherit pkgs lib config;};
      helix = import ./helix {inherit pkgs lib config inputs;};
    };
    xdg.userDirs = {
      enable = true;
      documents = "$HOME/Documents/";
      download = "$HOME/Downloads";
      videos = "$HOME/Videos";
      music = "$HOME/Music";
      pictures = "$HOME/Pictures";
    };
  };
}
