{
  pkgs,
  lib,
  config,
  inputs,
}:
lib.mkIf config.system.programs.helix.enable {
  enable = true;
  languages = [
    {
      name = "bash";
      language-server = {
        command = "${pkgs.nodePackages.bash-language-server}/bin/bash-language-server";
        args = ["start"];
      };
      auto-format = true;
    }
    {
      name = "dockerfile";
      roots = ["Dockerfile" "Containerfile"];
      file-types = ["Dockerfile" "Containerfile" "dockerfile" "containerfile"];
    }
    {
      name = "nix";
      language-server = {command = "${inputs.nil.packages.${pkgs.system}.default}/bin/nil";};
      formatter = {command = "${pkgs.alejandra}/bin/alejandra";};
      auto-format = true;
    }
    {
      name = "rust";
      language-server = {command = "${pkgs.rust-analyzer}/bin/rust-analyzer";};
      scope = "source.rust";
      injection-regex = "rust";
      file-types = ["rs"];
      roots = ["Cargo.toml" "Cargo.lock"];
      auto-format = false;
      comment-token = "//";
      indent = {
        tab-width = 3;
        unit = "    ";
      };
      formatter = {command = "${pkgs.rustfmt}/bin/rustfmt";};
    }
  ];
  settings = {
    theme = "nord";
    editor = {
      true-color = true;
      color-modes = true;
      cursorline = true;
      cursor-shape = {
        insert = "bar";
        normal = "block";
        select = "underline";
      };
      indent-guides = {
        render = true;
      };
      statusline.center = ["position-percentage"];
    };
    keys.normal = {
      space.u = {
        f = ":format"; # format using LSP formatter
        w = ":set whitespace.render all";
        W = ":set whitespace.render none";
      };
      space = {
        space = "file_picker";
        q = ":q";
        w = ":w";
      };
    };
  };
}
