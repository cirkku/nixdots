{
  config,
  pkgs,
  lib,
}:
lib.mkIf config.system.programs.direnv.enable {
  enable = true;
  nix-direnv.enable = true;
  enableZshIntegration = true;
}
