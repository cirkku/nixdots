{
  pkgs,
  lib,
  config,
}: let
  mkForum = boards: domain:
    builtins.map (name: {
      inherit name;
      url = "${domain}/${name}";
    })
    boards;
in
  lib.mkIf config.system.programs.firefox.enable {
    enable = true;
    extensions = with pkgs.nur.repos.rycee.firefox-addons; [
      decentraleyes
      ublock-origin
      clearurls
      sponsorblock
      bitwarden

      (buildFirefoxXpiAddon {
        pname = "image-search-options";
        version = "3.0.12";
        addonId = "{4a313247-8330-4a81-948e-b79936516f78}";
        url = "https://addons.mozilla.org/firefox/downloads/file/3059971/image_search_options-3.0.12.xpi";
        sha256 = "1fbdd8597fc32b1be11302a958ea3ba2b010edcfeb432c299637b2c58c6fd068";
        meta = with lib; {
          homepage = "https://saucenao.com";
          description = "A customizable reverse image search tool that conveniently presents a variety of top image search engines.";
          license = licenses.mpl11;
          platforms = platforms.all;
        };
      })
    ];
    package = pkgs.firefox-wayland;
    profiles.${config.system.username} = {
      bookmarks = [
        {
          name = "kernel.org";
          url = "https://www.kernel.org";
        }
        {
          name = "NixOS";
          url = "https://nixos.org";
        }
        {
          name = "Arch";
          url = "https://wiki.archlinux.org";
        }
        {
          name = "torrents";
          toolbar = true;
          bookmarks = [
            {
              name = "animebytes";
              url = "https://animebytes.tv";
            }
            {
              name = "nyaa";
              url = "https://nyaa.si";
            }
            {
              name = "myanonamouse";
              url = "https://myanonamouse.net";
            }
            {
              name = "bakabt";
              url = "https://bakabt.me";
            }
            {
              name = "rutorrent";
              url = "https://nibaab.pluto.usbx.me/rutorrent";
            }
          ];
        }
        {
          name = "4chan";
          toolbar = true;
          bookmarks = mkForum ["a" "c" "g"] "https://boards.4channel.org";
        }
        {
          name = "sushi";
          toolbar = true;
          bookmarks = mkForum ["kaitensushi" "silicon" "lounge"] "https://sushigirl.us";
        }
        {
          name = "youtube";
          toolbar = false;
          bookmarks = mkForum ["ZackFreedman" "NCommander" "Bisqwit" "DEATHFETISH"] "https://youtube.com/c";
        }
      ];
      settings = {
        "media.peerconnection.enabled" = false;
        "media.peerconnection.turn.disable" = true;
        "media.peerconnection.use_document_iceservers" = false;
        "media.peerconnection.video.enabled" = false;
        "media.peerconnection.identity.timeout" = 1;
        "privacy.firstparty.isolate" = true;
        "privacy.resistFingerprinting" = true;
        "privacy.trackingprotection.fingerprinting.enabled" = true;
        "privacy.trackingprotection.cryptomining.enabled" = true;
        "privacy.trackingprotection.enabled" = true;
        "browser.send_pings" = false;
        "browser.urlbar.speculativeConnect.enabled" = false;
        "dom.event.clipboardevents.enabled" = false;
        "media.navigator.enabled" = false;
        "network.cookie.cookieBehavior" = 1;
        "network.http.referer.XOriginPolicy" = 1;
        "network.http.sendRefererHeader" = 1;
        "network.http.referer.XOriginTrimmingPolicy" = 2;
        "beacon.enabled" = false;
        "browser.safebrowsing.downloads.remote.enabled" = false;
        "network.dns.disablePrefetch" = true;
        "network.dns.disablePrefetchFromHTTPS" = true;
        "network.predictor.enabled" = false;
        "network.predictor.enable-prefetch" = false;
        "network.prefetch-next" = false;
        "network.IDN_show_punycode" = true;
        "browser.newtabpage.activity-stream.feeds.section.topstories" = false;
        "browser.newtabpage.activity-stream.feeds.topsites" = false;
        "app.shield.optoutstudies.enabled" = false;
        "dom.security.https_only_mode_ever_enabled" = true;
        "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
        "browser.search.suggest.enabled" = false;
        "browser.urlbar.shortcuts.bookmarks" = true;
        "browser.urlbar.shortcuts.history" = false;
        "browser.urlbar.shortcuts.tabs" = false;
        "browser.urlbar.suggest.bookmark" = true;
        "browser.urlbar.suggest.engines" = false;
        "browser.urlbar.suggest.history" = true;
        "browser.urlbar.suggest.openpage" = false;
        "browser.urlbar.suggest.topsites" = false;
        "browser.uidensity" = 1;
        "media.autoplay.enabled" = true;
        "extensions.pocket.enabled" = false;
        "identity.fxaccounts.enabled" = false;
        "toolkit.zoomManager.zoomValues" = ".8,.95,1,1.1,1.2";
      };
      userChrome = "
            * { 
                box-shadow: none !important;
                border: 0px solid !important;
            }
        ";
    };
  }
