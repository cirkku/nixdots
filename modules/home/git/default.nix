{
  pkgs,
  lib,
  config,
}:
lib.mkIf config.system.programs.git.enable {
  enable = true;
  userName = "cirkku";
  userEmail = "tuukka.t.korhonen@protonmail.com";
  extraConfig = {
    init = {defaultBranch = "main";};
  };
}
