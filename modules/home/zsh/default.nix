{
  config,
  pkgs,
  lib,
}:
lib.mkIf config.system.programs.zsh.enable {
  enable = true;
  dotDir = ".config/zsh";
  initExtra = ''
    PROMPT='%(?.%F{green}√.%F{red}?%?)%f %B%F{240}%1~%f%b %# '

    # Clean up
    export XAUTHORITY="$HOME/.Xauthority"
    export GEM_HOME="$HOME/.local/share/gem"
    export LESSHISTFILE=-

    # I honestly don't know what this does
    autoload -U colors && colors
    eval "$(dircolors -b)"
    setopt histignorealldups sharehistory

    # Enable completion
    autoload -Uz compinit
    zstyle ":completion:*" menu select
    zstyle ":completion:*" list-colors ""
    zmodload zsh/complist
    compinit -d $HOME/.cache/zcompdump
    _comp_options+=(globdots)

    #enable zsh internal options
    setopt correct
    setopt globdots
    setopt ignorebraces
    setopt autocd

    # Set vi-mode and bind ctrl + space to accept autosuggestions
    bindkey '^ ' autosuggest-accept
    bindkey '^A' vi-beginning-of-line
    bindkey '^E' vi-end-of-line

  '';

  # Tweak settings for history
  history = {
    save = 2000;
    size = 2000;
    path = "$HOME/.cache/zsh_history";
  };

  # Set some aliases
  shellAliases = {
    rcd = ". ranger";
    h = "hx";
    c = "clear";
    tarx = "tar -xvzf";
    mkdir = "mkdir -vp";
    rm = "rm -rifv";
    mv = "mv -iv";
    cp = "cp -riv";
    cat = "bat --paging=never";
    fzf = "fzf --preview 'bat --color=always --style=numbers --line-range=:500 {}'";
    ls = "exa -a --icons";
    tree = "exa --tree --icons";
  };
  # Source all plugins
  plugins = [
    {
      name = "zsh-syntax-highlighting";
      src = pkgs.fetchFromGitHub {
        owner = "zsh-users";
        repo = "zsh-syntax-highlighting";
        rev = "b2c910a85ed84cb7e5108e7cb3406a2e825a858f";
        sha256 = "lxwkVq9Ysvl2ZosD+riQ8dsCQIB5X4kqP+ix7XTDkKw=";
      };
    }
    {
      name = "auto-ls";
      src = pkgs.fetchFromGitHub {
        owner = "notusknot";
        repo = "auto-ls";
        rev = "225de50a3f4108e53526582a76e7325fc31cc38f";
        sha256 = "5HN6Xj8myZLZVZ/BREue7ITRHA5LCvVZ5UcyWSbagEU=";
      };
    }
    {
      name = "zsh-autosuggestions";
      src = pkgs.fetchFromGitHub {
        owner = "zsh-users";
        repo = "zsh-autosuggestions";
        rev = "a411ef3e0992d4839f0732ebeb9823024afaaaa8";
        sha256 = "KLUYpUu4DHRumQZ3w59m9aTW6TBKMCXl2UcKi4uMd7w=";
      };
    }
  ];
}
