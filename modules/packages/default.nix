{
  pkgs,
  lib,
  config,
  inputs,
  ...
}: {
  config = {
    home-manager.users.${config.system.username} = {
      home.packages = [
        pkgs.ripgrep
        pkgs.exa
        pkgs.bat
        pkgs.fzf
        pkgs.ipmitool
        pkgs.yt-dlp
        pkgs.nix-prefetch-github
        pkgs.update-nix-fetchgit
        pkgs.imagemagick
        pkgs.git
        pkgs.wget
        pkgs.curl
        (pkgs.discord.override {withOpenASAR = true;})
        inputs.prism-launcher.packages.${pkgs.system}.prismlauncher
        pkgs.gzdoom
        pkgs.lutris
        pkgs.fceux
        pkgs.dolphin-emu
        pkgs.pcsx2
        pkgs.rpcs3
        inputs.nix-gaming.packages.${pkgs.system}.wine-ge
        pkgs.winetricks
        pkgs.yuzu-mainline
      ];
    };
  };
}
