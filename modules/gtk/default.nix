{
  pkgs,
  lib,
  config,
  inputs,
  ...
}:
with lib; let
  cfg = config.system.desktop.gtk;
  my-colloid = pkgs.colloid-gtk-theme-local.override {
    themeVariants = ["teal"];
    colorVariants = ["dark"];
    tweaks = ["nord"];
  };
  my-colloid-icons = pkgs.colloid-icon-theme.override {
    schemeVariants = ["nord"];
    colorVariants = ["teal"];
  };
in {
  config = mkIf cfg.enable {
    home-manager.users.tuukka = {
      gtk = {
        enable = true;
        iconTheme = {
          name = "Colloid-teal-nord-dark";
          package = my-colloid-icons;
        };
        theme = {
          name = "Colloid-Teal-Dark-Nord";
          package = my-colloid;
        };
        gtk3.extraConfig = {
          Settings = ''
            gtk-application-prefer-dark-theme=1
          '';
        };
        gtk4.extraConfig = {
          Settings = ''
            gtk-application-prefer-dark-theme=1
          '';
        };
      };

      # HACK TO GET LIBADWAITA WORKING
      home.file."gtk.css" = {
        text = builtins.readFile "${my-colloid}/share/themes/Colloid-Teal-Dark-Nord/gtk-4.0/gtk.css";
        target = "/.config/gtk-4.0/gtk.css";
      };
    };
  };
}
