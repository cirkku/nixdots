{
  description = "bad flake configuration";
  inputs = {
    prism-launcher.url = "github:PrismLauncher/PrismLauncher";
    nix-gaming.url = "github:fufexan/nix-gaming";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    unstable.url = "github:nixos/nixpkgs?rev=1d1ed8fbb244fe3d242ebd3d2ad4e647235719eb";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nil = {
      url = "github:oxalica/nil";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nur.url = "github:nix-community/NUR";
    utils.url = "github:gytis-ivaskevicius/flake-utils-plus";
  };
  outputs = inputs @ {
    self,
    home-manager,
    nixpkgs,
    nur,
    nix-gaming,
    prism-launcher,
    nil,
    utils,
    unstable,
  }: let
    mkStandardHosts = hostnames:
      nixpkgs.lib.genAttrs hostnames (hostname: {
        modules = [
          ./hosts/${hostname}/default.nix
          ./hosts/${hostname}/hardware.nix
          ./hosts/${hostname}/filesystems.nix
        ];
      });
  in
    utils.lib.mkFlake {
      inherit self inputs;

      supportedSystems = ["x86_64-linux" "aarch64-linux"];
      channelsConfig = {
        allowBroken = true;
        allowUnfree = true;
      };

      sharedOverlays = [
        self.overlay
        nur.overlay
      ];

      hostDefaults = {
        modules = [
          home-manager.nixosModules.home-manager
          ./modules
          ./hosts
        ];
        extraArgs = {
          inherit inputs;
        };
      };
      hosts = mkStandardHosts ["desktop" "laptop"];

      outputsBuilder = channels:
        with channels.nixpkgs; {
          packages = {
            inherit
              appmenu-gtk-module
              fildem
              colloid-gtk-theme-local
              ;
          };
          devShell = mkShell {
            name = "dotfiles";
            packages = [
              alejandra
              git
            ];
          };
        };
      overlay = import ./overlays;
    };
}
